<<<<<<< HEAD
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Acceuil</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css"
          rel="stylesheet"
          integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi"
          crossorigin="anonymous">
    <style>
        body{
            background: linear-gradient(to bottom right, #fff, #e2005c);
        }

        .header{
            width: 100%;
            height: 80px;
            background-color: #fff;
            display: flex;
            justify-content: space-between;
            align-items: center;
            padding: 20px;
        }

        .header-logo{
            font-size: 25px;
            font-weight: bold;
        }

        .content{
            width: 100%;
            min-height: calc(100vh - 400px);
            margin-top: 100px;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        .options{
            width: 100%;
            height: 200px;
            background-color: #fff;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        footer{
            width: 100%;
            height: 120px;
            background-color: #515151;
            color: #fff;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        .footer-text{
            text-align: center;
        }
        h1 {
  text-align: center;
  margin-top: 0;
}
    </style>
    <link rel="stylesheet" href="style.css"/>
</head>
<body>
<header class="header">
    <div class="header-logo">Esigelec</div>
    <nav class="navbar navbar-expand-md ">
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarText">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="account.php">Account</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="/includes/deconnexion.php">Deconnexion</a>
                </li>
             </ul>
        </div>
    </nav>
</header>

<div class="content text-center">
    <?php
    if ($_SESSION['nom']) {
        echo '<div class="alert alert-primary alert-dismissible fade show" role="alert">';
        echo $_SESSION['message'];
        echo '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>';
        echo '</div>';
        unset($_SESSION['message']);
    }
    ?>

    <?php
    session_start();
    $nom = $_SESSION['nom'];
    $badge = $_SESSION['badge'];
    ?>
    <h1>Bienvenue Monsieur <?php echo $nom; ?></h1>
       
</div>

<div class="options">
    <div class='col  d-flex justify-content-center'>
        <a style="margin-right: 24%;" href="./inscription.php" class='btn btn-primary'>Créer un compte pour un utilisateur</a>
    </div>
    <div class='col  d-flex justify-content-center'>
        <a style="margin-right: 24%;" href="./liste_utilisateur.php" class='btn btn-primary'>Gérer les utilisateurs</a>
    </div>
</div>

<footer>
    <div class="footer-text">&copy; Esigelec - Projet réseau</div>
</footer>

<script
        src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3"
        crossorigin="anonymous">
</script>
</body>
</html>
=======

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Acceuil</title>
    <link 
    href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" 
    rel="stylesheet" 
    integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" 
    crossorigin="anonymous">
    <link rel="stylesheet" href="style.css" />
  </head>
  <body > 
    
            <?php
                 include_once('includes/header.php');
                //  session_start();
                $titre = "Acceuil administrateur";
                    if($_SESSION['nom']) {
                      echo '<div class="alert alert-primary alert-dismissible fade show" role="alert">';
                      echo $_SESSION['message'];
                      echo '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>';
                      echo '</div>';
                       unset($_SESSION['message']);
                  }
                
            ?>

  

        <div class="container-fluid">
          
           
                <div class="row d-flex justify-content-center">
                    
                    <div class="col-md-4 col-lg-6 border border-white border-2 rounded " style=" ">   
                        <a href="inscription.php" class="text-dark"><strong>Créer une compte pour un utilisateur</strong> </a> 
                    </div>
                    
                </div>
                
                <div class="row justify-content-center">
                    <div class="col-md-4 col-lg-6 border border-white border-2 rounded " style=" ">
                        <a href="modifier.php" class="text-dark"><strong>Modifier un compte utilisateur</strong> </a>   
                    </div>
                    
        </div>
                                
        <?php include_once('../includes/footer.php'); ?>
    
  </body>
    <script 
    src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" 
    integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" 
    crossorigin="anonymous">
    </script>
</html>
>>>>>>> 435be901a97427d94d1f32b38741d0c0885fe12f
