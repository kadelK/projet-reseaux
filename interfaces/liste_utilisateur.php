<?php
session_start();

// Connexion à la base de données
$bdd = new PDO('mysql:host=localhost;dbname=reseau_bdd;charset=utf8', 'user', 'pass');

// Récupération de tous les utilisateurs
$req = $bdd->query('SELECT * FROM utilisateur WHERE role != 1');

?>

<!DOCTYPE html>
<html>
  <head>
    <title>Tableau d'utilisateurs</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
  </head>
  <body>
    <table class="table table-striped">
      <thead>
        <tr>
          <th>Nom</th>
          <th>Prénom</th>
          <th>Rôle</th>
          <th>Statut</th>
          <th>Solde</th>
          <th>Badge</th>
          <th>Acion </th>
        </tr>
      </thead>
      <tbody>
        <tr>
        <?php while ($utilisateur = $req->fetch() ) { ?>
          <tr>
            <td><?php echo $utilisateur['nom']; ?></td>
            <td><?php echo $utilisateur['prenom']; ?></td>
            <td><?php  if ( $utilisateur['role']==2) {
             echo "utilisateur";
            }else {
              echo "Commercant";
            }?></td>
            <td><?php if ($utilisateur['statut']==1) {
              echo "Actif";
            } else{
              echo "inactif";
            } ?></td>
            <td><?php echo $utilisateur['solde']; ?></td>
            <td><?php echo $utilisateur['rfid_badge']; ?></td>
            <td>
              <a href="/includes/modifier_utilisateur.php?id=<?php echo $utilisateur['rfid_badge']; ?>" class="btn btn-primary">Modifier</a>
                <?php if ($utilisateur['statut'] == 1) { ?>
    <a href="/includes/desactiver.php?id=<?php echo $utilisateur['rfid_badge']; ?>" class="btn btn-danger">Désactiver</a>
  <?php } else { ?>
    <a href="/includes/activer.php?id=<?php echo $utilisateur['rfid_badge']; ?>" class="btn btn-success">Activer</a>
  <?php } ?>            
              <a  class="btn btn-success" href="/includes/affecter.php?id=<?php echo $utilisateur['rfid_badge']; ?>">Associer badge</a>
            </td>
          </tr>
        <?php } ?>
         
        </tr>
      </tbody>
    </table>
  </body>
</html>