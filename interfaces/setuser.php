
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Acceuil</title>
    <link 
    href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" 
    rel="stylesheet" 
    integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" 
    crossorigin="anonymous">
    <link rel="stylesheet" href="style.css" />
  </head>
  <body > 
    
            <?php
                 include_once('includes/header.php');
                //  session_start();
                $titre = "Modification des informations utilisateur";
            
                
            ?>

  

        <div class="container-fluid">
          
           
                <div class="row d-flex justify-content-center">
                    
                    <div class="col-md-4 col-lg-6 border border-white border-2 rounded " style=" ">   
                        <a href="inscription.php" class="text-dark"><strong>Créer une compte pour un utilisateur</strong> </a> 
                    </div>
                    
                </div>
                
                <div class="row justify-content-center">
                    <div class="col-md-4 col-lg-6 border border-white border-2 rounded " style=" ">
                        <a href="setutser.php" class="text-dark"><strong>Modifier un compte utilisateur</strong> </a>   
                    </div>
                    
        </div>
                                
        <?php include_once('../includes/footer.php'); ?>
    
  </body>
    <script 
    src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" 
    integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" 
    crossorigin="anonymous">
    </script>
</html>