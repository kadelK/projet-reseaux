<?php
// Connexion à la base de données
require_once("param.inc.php");
$mysqli = new mysqli($host, $name, $passwd, $dbname);
if ($mysqli->connect_error) {
    die('Erreur de connexion (' . $mysqli->connect_errno . ') '
            . $mysqli->connect_error);
}

// Récupération de l'ID de l'utilisateur à désactiver
$id_utilisateur = $_GET['id'];

// Mise à jour du statut de l'utilisateur à "inactif" dans la base de données
$desactiver = $mysqli->query("UPDATE utilisateur SET statut = '1' WHERE rfid_badge = '$id_utilisateur'");
if (!$desactiver) {
    die ("Echec de la requête : ".$mysqli->error);
}else{
    echo "good";
}

// Redirection vers la page précédente
header("Location: ".$_SERVER['HTTP_REFERER']);
?>
