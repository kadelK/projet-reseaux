<?php
// Connection config variables:
$servername = "localhost";
$username   = "root";
$password   = "root";
$dbname     = "reseau_bdd";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

if (isset($_POST['username'])) {
    // Get values from form 
    $username = $_POST['username'];
    $password = $_POST['password'];

    // Query to check if username exists
    $query    = "SELECT * FROM utilisateur WHERE nom = '$username'";
    $result   = mysqli_query($conn, $query);
    $row      = mysqli_fetch_assoc($result);
    $role     = $row['role'];
    $hashPass = $row['mot_de_pass'];

    // validate the username and password 
    if (password_verify($password, $hashPass)) {
        // Success
        session_start();
        $_SESSION['loggedin'] = true;
        $_SESSION['username'] = $username;

        if($role == 1) {
            header('location: accueil_admin.php');
        } else {
            header('location: accueil_utilisateur.php');
        }
        
    } else {
        // Failed 
        echo "Username/Password incorrect.";
    }
}
?>