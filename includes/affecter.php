<?php
session_start();
$titre = "Payment";

// Vérifier si l'ID de l'utilisateur est défini
if (isset($_GET['id'])) {
    $id_utilisateur = $_GET['id'];

    // Vérifier si le tag RFID est détecté
    $tag = shell_exec('/home/pi/script/tag_detect.sh');
    if ($tag != null) {
        // Connexion à la base de données
        require_once("param.inc.php");
        $mysqli = new mysqli($host, $name, $passwd, $dbname);
        if ($mysqli->connect_error) {
            die('Erreur de connexion (' . $mysqli->connect_errno . ') '
                    . $mysqli->connect_error);
        }
              $badge =trim($tag);
        // Mettre à jour la valeur de rfid_badge pour l'utilisateur
        $affect = $mysqli->query("UPDATE utilisateur SET rfid_badge = '$badge' WHERE rfid_badge = '$id_utilisateur'");
        if (!$affect) {
            die ("Echec de la requête : ".$mysqli->error);
        } else {
            $_SESSION['message'] = " succeeded";
            $_SESSION['newbadge'] = $tag;
            header("Location: ".$_SERVER['HTTP_REFERER']);
            // Redirection vers la page précédente
        }
    }
}
?>
