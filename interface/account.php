<?php
  // session_start();
  $titre = "Mon compte";   
?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Acceuil</title>
    <link 
    href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" 
    rel="stylesheet" 
    integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" 
    crossorigin="anonymous">
    <link rel="stylesheet" href="style.css" />
  </head>

  <body>
  <?php include_once('../includes/header.php'); ?>

    <div class="container">
      <div class="row">
        <div class="col" > 
          <?php echo 'Nom: ' .$_SESSION['nom'];?>
        </div>
      </div>

      <div class="row">
        <div class="col " style="">Prenom : <?php echo 'Prenom:'.$_SESSION['prenom'];?></div>
      </div>
      <div class="row">
        <div class="col" style="">Nom : <?php echo 'nom:'.$_SESSION['nom'];?></div>
      </div>
      <div class="row">
        <div class="col" style="">Badge RFID : <?php echo 'badgeRFID:'.$_SESSION['badgeRFID'];?></div>
      </div>
      

    </div>
    
           <script 
            src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" 
            integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" 
            crossorigin="anonymous">
            </script>
  </body>
<?php include_once('../includes/footer.php'); ?>

</html>

  
